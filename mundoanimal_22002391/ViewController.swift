//
//  ViewController.swift
//  mundoanimal_22002391
//
//  Created by COTEMIG on 17/11/22.
//

import UIKit
import Kingfisher
import Alamofire

struct Animal: Decodable {
    let name: String
    let latin_name: String
    let image_link : String
    
}

class ViewController: UIViewController {
    @IBOutlet weak var image_link: UIImageView!
    @IBOutlet weak var latin_name: UILabel!
    @IBOutlet weak var name: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNovoAnimal()
    }
@IBAction func recarregarImagem(_ sender: Any) {
    getNovoAnimal()
}

func getNovoAnimal(){
    AF.request("https://zoo-animal-api.herokuapp.com/animals/rand").responseDecodable(of: Animal.self) { response in
            if let Animal = response.value {
               
                self.image_link.kf.setImage(with: URL(string: Animal.image_link))
                self.name.text = Animal.name 
                self.latin_name.text = Animal.latin_name
                
            }
        
    }
    
}



}


